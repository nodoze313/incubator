#include <Arduino.h>

//#define BME_ENABLE

// BME
#ifdef BME_ENABLE
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1013.25) // sea level pressure for BME

#endif

#include <OneWire.h>
#include <DallasTemperature.h>
//#include <PWM.h>


#include <EEPROM.h>


// Humidity Servo
#include <Servo.h>  // Is this right, or should it be a PWM servo?  Maybe this and the fan can use the same library?

//pins
#define FAN_PORT A1

#define VOCORE_POWER 6

#define TEMP_BUS 3

#define ROTATE_RELAY 7

#define HEAT_RELAY 8

#define HUMIDITY_SERVO 9 // servo motor for humidity on digital pin

#ifdef BME_ENABLE

Adafruit_BME280 bme;
#define BME_SCK 13  // I2C clock SCL pin digital 2 on micro, any digital pin
#define BME_MISO 12 // any digital pin
#define BME_MOSI 11 // and digital pin
#define BME_CS 10   // any digital pin

#endif

OneWire tempWire(TEMP_BUS);
DallasTemperature tempSensor(&tempWire);

// Temperature is in C
// Pressure is in pascals 100 pascals = 1 hpa = 1 millibar

Servo humidityServo;

int delayDuration   = 10;
int rotateDuration  = 2;

int desiredHumidityLow  = 50; // initial starting humidity, vocore2 updates this
int desiredHumidityHigh = 55; // initial starting humidity, vocore2 updates this
int currentHumidity     = 0;  // servo position default

int desiredTemperature  = 100;

static int eepromDefined = 16;

#ifdef BME_ENABLE
int readTemp() {
  tempSensor.requestTemperatures();
  int tempSensorV = int( floor( tempSensor.getTempFByIndex(0) ) );
  Serial.print("TempSensorV: ");
  Serial.println(tempSensorV);
  int bmeTempV = bme.readTemperature();
  Serial.print("BMETempV: ");
  Serial.println(bmdTempV);
  int outTemp = ( tempSensorV + bmeTempV ) / 2;
  return outTemp;
}
int readHumidity() {
  return bme.readHumidity();
}
#else
int readTemp() {
  tempSensor.requestTemperatures();
  int temp = int( floor( tempSensor.getTempFByIndex(0) ) );
  return temp;
}
int readHumidity() {
  return 50;
}
#endif

void setHeatOff() {
  if(Serial) {
    Serial.println("heat off");
  }
  digitalWrite( HEAT_RELAY, HIGH);
}
void setHeatOn() {
  if(Serial) {
    Serial.println("heat on");
  }
  digitalWrite( HEAT_RELAY, LOW );
}

void doRotate() {
  digitalWrite( ROTATE_RELAY, HIGH );
  delay( (  rotateDuration * 1000 ) );
  digitalWrite( ROTATE_RELAY, LOW );
}

void setTemp() {
  int actualTemp = readTemp();
  if(Serial) {
    Serial.println("setTemp");
    Serial.print("actualTemp ");
    Serial.print(actualTemp);
    Serial.print(" desired temp  ");
    Serial.println(desiredTemperature);
  }
  if ( desiredTemperature < actualTemp ) { // if target temp is gt actual off
    setHeatOff();
  } else {
    setHeatOn();
  }
}

void setHumidity() {
// humidity check
  int actualHumidity = readHumidity();
  int humidityNew = currentHumidity;
  if(Serial) {
    Serial.println("setHumidity");
  }
  if ( desiredHumidityLow < actualHumidity ) {
    humidityNew += 1; // increase humidity
  } else if ( desiredHumidityHigh > actualHumidity ) {
    humidityNew -= 1; // decrease humidity
  }
  humidityServo.write( humidityNew );
  currentHumidity = humidityNew;
}

void cycle() {
  setTemp();
  setHumidity();
}

void checkSerial() {
  int serin = 0;
  bool done = false;
  bool writeChanges = false;
  while( ! done ) {
    serin  = Serial.read();
    switch( serin ) {
      case -1:
          done = true;
        break;
      case 1: // rotate instruction
        doRotate();
        break;
      case 2: // humidity low read
        desiredHumidityLow = Serial.read();
        writeChanges = true;
        break;
      case 3: // humidity high read
        desiredHumidityHigh = Serial.read();
        writeChanges = true;
        break;
      case 4: // temperature read
        desiredTemperature = Serial.read();
        writeChanges = true;
        break; 
      case 5: // delayDuration read
        delayDuration = Serial.read();
        writeChanges = true;
        break; 
      case 6: // rotateDuration read
        rotateDuration = Serial.read();
        writeChanges = true;
        break; 
    }
  }
  if( writeChanges ) {
    saveConfig();
  }
}

void saveConfig() {
  Serial.println("saveConfig");
  EEPROM.write( 0, eepromDefined );
  EEPROM.write( 2, desiredHumidityLow );
  EEPROM.write( 3, desiredHumidityHigh );
  EEPROM.write( 4, desiredTemperature );
  EEPROM.write( 5, delayDuration );
  EEPROM.write( 6, rotateDuration );
}

void loadConfig() {
  Serial.println("loadConfig");
  if( EEPROM.read( 0 ) == eepromDefined ) {
    desiredHumidityLow  = EEPROM.read( 2 );
    desiredHumidityHigh = EEPROM.read( 3 );
    desiredTemperature  = EEPROM.read( 4 );
    delayDuration       = EEPROM.read( 5 );
    rotateDuration      = EEPROM.read( 6 );
  } else {
    saveConfig();
  }
}

void setup() {
  Serial.begin(57600);
  loadConfig();
  if(Serial) {
    Serial.println("Incubartor v0.1");
  }

  // BME
#ifdef BME_ENABLE
  if( ! bme.begin()) {
    Serial.println("Failed to start BME sensor");
  }
#endif

  // vocorePower on
  pinMode( VOCORE_POWER, OUTPUT );
  digitalWrite( VOCORE_POWER, HIGH );

  // tempSensor
  pinMode( TEMP_BUS, INPUT);

  // rotator
  pinMode( ROTATE_RELAY, OUTPUT );
  digitalWrite( ROTATE_RELAY, LOW );

  // heat light
  pinMode( HEAT_RELAY, OUTPUT );
  digitalWrite( HEAT_RELAY, LOW );

  // humidity servo
  humidityServo.attach( HUMIDITY_SERVO );
}

int delayPos = 0;
void loop() {
  delay( ( delayDuration * 1000 ) );
  checkSerial(); // read instructions from vocore2
  cycle(); // do a cycle test
}


