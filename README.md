There are lots of incubators, why ya?  It's pretty simple and I like doing things myself.


Parts:
- microwave
- egg holder grid - grid that fits above rotator but not touching
- arduino 
- vocore2
- 2x relays ( rotator & heater )
- 2x glade plugins for resistor heater b/c they're free
- BME humidity temperature sensor
- 1x servo

Operation:
 - the arduino holds the temperature and humidity constant turning on and off the elements
 - the vocore2 dictates the current state control for the arduino
 - microwave provides rotation mech and fan
 - glade provides heat source 
 - servo raises and lowers a spounge out of a jar of water to change humidity


Heater calculation:
 - 1^3 ft of space
 - 6 air changes / hr
 - R value 15
 - Outside temp 70
 - Interior temp 100
 - 9.42BTU/hr = 2.7W/hr
 - glade resistors are 6W each


