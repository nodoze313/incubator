usb=/dev/ttyS1
lock=/tmp/serlock
# ttyS0 is nc
# ttyS1 is vocore serial to arduino
# ttyS2 is the vocore2 serial to computer

to= #mail to
from=incbator@localhost # mail from
smtp= # smtp server
smtp_port=587 # smtp port
subject="Incubator Status"

stty -F $usb raw speed 57600 -cstopb -parity -echo

sendln() {
  while [ -f $lock ]; do sleep 1; done
  touch $lock
  echo -ne "\x$1" > $usb
  rm -f $lock
}
readln() {
  while [ -f $lock ]; do sleep 1; done
  touch $lock
  timeout 40 head -n 30 $usb
  if [[ $? -gt 0 ]]; then # timeout returns 124 when it times out, otherwise 0
    echo "arduino read error"
  fi
  rm -f $lock
}


# chicken eggs must be incubated at 100-102
# takes 21 days
# 1-17 101dF,  50-55% humidity
# 18-19 100dF, 55-60% humidity
# 20-21 100dF, 65-70% humidity
#
# the default state of the arduino is 1-17, but always set it on startIncubate
# this script runs the process keeping state on controller outside of arduino
# 

start_file=/etc/incubation_start
rotate_last=/etc/incubation_rotate

# set everything to defaults and begin process
startIncubate() {
  touch $start_file
  rotate
  phase1
}

rotate() {
  sendln 1
}

setState() {
  humidityLow=$1
  humidityHigh=$2
  temperature=$3
  # humidity low set
  sendln 2
  sendln $humidityLow
  # humidity high set
  sendln 3
  sendln $humidityHigh
  # temperature set
  sendln 4
  sendln $temperature
}

phase1() {
  setState 32 37 64
}
phase2() {
#TODO only do this once
#TODO disable turning
  setState 37 3c 63
}
phase3() {
#TODO only do this once
  setState 41 46 63
}

currentDay() {
  touch -d "$1 days ago" /tmp/dt
  if [[ /tmp/dt -nt $start_file ]]; then
    echo "Found phase change, doing $2" >> ~/log
    $2
  fi
}

# run every hour
checkRotate() {
  [[ $(( $(date +%H) % 4 )) -eq 0 ]] && rotate
  currentDay 19 phase3
  currentDay 17 phase2
}

# run every minute
readArduino() {
  #readln | logger
  readln | grep actualTemp | cut -f 2 -d " " | while read temp; do
    if [[ $temp -lt 99 ]]; then
      alert "Low Temperature"
    fi
  done
}

alert() {
  echo "$1" | mailsend -t $to  -sub $subject -f $from -smtp $smtp -port $smtp_port
}

$@

